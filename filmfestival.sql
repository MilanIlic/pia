-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 21, 2018 at 12:43 PM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `filmfestival`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `username` varchar(20) NOT NULL,
  `film` varchar(40) NOT NULL,
  `comment` text,
  PRIMARY KEY (`username`,`film`),
  KEY `film` (`film`),
  KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`username`, `film`, `comment`) VALUES
('milani', 'Jumanji: Welcome to the Jungle', 'Ekstra film. Preporuka!'),
('milani', 'Matilda', 'Odlican Film!'),
('milani', 'Wonder', 'Preporuka, odusevljen sam!'),
('urosv', 'Jumanji: Welcome to the Jungle', 'Ekstra film!');

-- --------------------------------------------------------

--
-- Table structure for table `festival`
--

DROP TABLE IF EXISTS `festival`;
CREATE TABLE IF NOT EXISTS `festival` (
  `name` varchar(50) NOT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `place` varchar(20) DEFAULT NULL,
  `about` text CHARACTER SET latin2 COLLATE latin2_croatian_ci,
  `maxRez` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `XPKFestival` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `festival`
--

INSERT INTO `festival` (`name`, `startDate`, `endDate`, `place`, `about`, `maxRez`) VALUES
('46. Fest', '2017-12-31', '2017-12-31', 'Beograd', 'Prvi FEST pod naslovom Hrabri novi svet otvoren je 9. januara 1971. u Domu Sindikata', NULL),
('Beogradski festival', '2018-10-10', '2018-10-20', 'Beograd', 'Najbolji festival u regionu vec 45 godina', 15),
('FEST17', '2017-02-24', '2017-03-05', 'Beograd', 'FEST je nezaobilazna tacka u kulturnom zivotu Beograda na kome se vec cetiri decenije prikazuje sve ono najvrednije iz svetske i domace kinematografije.', 5),
('Festival Autorskog Filma', '2017-11-23', '2017-12-03', 'Beograd', 'Prvi Festival autorskog filma odrzan je 7-14. decembra 1994. u Beogradu, u organizaciji Jugoslavija-filma.\r\n\r\nOsnivac i prvi direktor festivala: Vojislav Vucinic\r\n\r\nPrvi umetnicki direktor festivala: Slobodan Novakovic', 5),
('Filmski Festival Herceg Novi', '2018-08-01', '2018-08-07', 'Herceg Novi', 'Trideset godina postojanja Festival docekuje kao jedna od najposjecenijih i najvazinijih kulturnih manifestacija u Herceg Novom, i dalje otvoren za nove ideje i transformacije.', 3),
('Hercegnovski filmski festival', '2017-12-31', '2017-12-31', 'Herceg Novi', 'Jugoslovenski filmski festival osnovan je 1986. godine, a danasnji naziv nosi od 2002. godine', NULL),
('Martovski festival', '2018-03-28', '2019-04-01', 'Beograd', 'Martovski festival (Beogradski festival dokumentarnog i kratkometražnog filma), jedan od najdugovečnijih evropskih filmskih festivala, svoje jubilarno izdanje dočekuje u trenutku najvećeg uspeha srpskog dokumentarnog filma, odnosno glavne nagrade za Milu Turajlić u Amsterdamu. Nadovezujući se na višegodišnji uspešni koncept stvaranja uslova za proizvodnju domaćih dokumentarnih, kratkometražnih igranih, animiranih i eksperimentalnih filmova Filmskog centra Srbije, Martovski festival u organizaciji Doma omladineBeograda, sa Markom Popovićem, rediteljem i producentom, kao umetničkim direktorom i Bobanom Jevtićem, dramaturgom, kao specijalnim savetnikom, biće 65. put održan od 28. marta do 1. aprila 2018. godine.', 4),
('Novo Beogradski Festival', '2018-01-03', '2018-01-24', 'Novi Beograd', 'Novi festival na Novom Beogradu!', 10),
('Omladinski', '2018-01-23', '2018-01-27', 'Beograd', 'Festival za omladinu!', 5);

-- --------------------------------------------------------

--
-- Table structure for table `film`
--

DROP TABLE IF EXISTS `film`;
CREATE TABLE IF NOT EXISTS `film` (
  `name` varchar(40) CHARACTER SET latin1 NOT NULL,
  `serbianName` varchar(40) CHARACTER SET latin1 DEFAULT NULL,
  `picture` varchar(40) CHARACTER SET latin1 DEFAULT NULL,
  `director` varchar(40) CHARACTER SET latin1 DEFAULT NULL,
  `duration` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `yearFilm` int(11) DEFAULT NULL,
  `aboutFilm` text CHARACTER SET latin1,
  `country` varchar(40) CHARACTER SET latin1 DEFAULT NULL,
  `imdb` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `rotten` varchar(150) COLLATE utf16_bin DEFAULT NULL,
  `youtube` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `casts` varchar(200) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `XPKFilm` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_bin;

--
-- Dumping data for table `film`
--

INSERT INTO `film` (`name`, `serbianName`, `picture`, `director`, `duration`, `yearFilm`, `aboutFilm`, `country`, `imdb`, `rotten`, `youtube`, `casts`) VALUES
('Atomski zdesna', 'Atomski zdesna', NULL, 'Srdjan Dragojevic', '94 min', 2014, '/', 'Serbia,Montenegro', 'http://www.imdb.com/title/tt3188768', '/', NULL, 'Srdjan Todorovic,Tanja Ribic,Branko Djuric'),
('Dama Pik', 'Pikova dama', NULL, 'Pavel Lungin', '120 min', 2016, 'Once upon a time, the great soprano Sophia Maier conquered the world with her voice, her beauty and the legend she carefully built around herself. Now only the legend remains- the diva herself hasn\'t performed for years, nor been seen in the glittering circles of society she once dominated. But the woman who fascinated and thrilled the world for so long would like to crown her career with one more triumph. And she\'ll use every dirty trick she knows to achieve it.', 'Russia', 'http://www.imdb.com/title/tt2231457/', 'https://www.rottentomatoes.com/m/pikovaya_dama_2013', NULL, 'Kseniya Rappoport,Ivan Yankovkiy,Igor Mirkurbanov'),
('Jumanji: Welcome to the Jungle', 'DŽUMANDŽI: DOBRODOŠLI U DŽUNGLU', 'jumanji', 'Jake Kasdan', '119min', 2017, 'Cetvoro srednjoskolaca otkrivaju staru video igru.', 'USA', 'http://www.imdb.com/title/tt2283362/?ref_=nv_sr_1', 'https://www.rottentomatoes.com/m/jumanji_welcome_to_the_jungle/', 'https://www.youtube.com/embed/leE10vdvkho', '	Dwayne Johnson, Jack Black, Kevin Hart, Karen Gillan'),
('La reina de Espana', 'Kraljica Spanije', NULL, 'Fernando Trueba', '128 min', 2016, '/', 'Spain', 'http://www.imdb.com/title/tt2411144', 'https://www.rottentomatoes.com/m/the_queen_of_spain', NULL, 'Penelope Cruz,Cary Elwes,Mandy Patnikin,Antonio Resines'),
('Malena', 'Malena', NULL, 'Giuseppe Tornatore', '109 min', 2000, 'Amidst the war climate, a teenage boy discovering himself becomes love-stricken by MalÃ¨na, a sensual woman living in a small, narrowminded Italian town.', 'Italia/USA', 'http://www.imdb.com/title/tt0213847', ' https://www.rottentomatoes.com/m/1101561_1101561_malena', 'https://www.youtube.com/embed/Knu5zKiK5hw', 'Monica Bellucci,Giuseppe Sulfaro,Luciano Federico'),
('Matilda', 'Matilda', 'matilda', 'Aleksey Uchitel', '108 min', 2017, 'U sumrak carske Rusije rodila se jedna od ljubavi koje menjaju svet.', 'Rusija', 'http://www.imdb.com/title/tt4419196/', 'https://www.rottentomatoes.com/m/mathilde/', 'https://www.youtube.com/embed/iflRh3c7brM', 'Michalina Olszanska, Lars Eidinger, Luise Wolfram'),
('Murder on the Orient Express', 'UBISTVO U ORIJENT EKSPRESU', 'orient', 'Kenneth Branagh', '114 min', 2017, 'Ono sto je izgledalo da ce biti raskosno putovanje vozom kroz Evropu, ubrzo se pretvorilo u jednu od najglamuroznijih, najnapetijih i najuzbudljivijih misterija svih vremena.', 'USA', 'http://www.imdb.com/title/tt3402236/?ref_=nv_sr_1', 'https://www.rottentomatoes.com/m/murder_on_the_orient_express_2017', 'https://www.youtube.com/embed/Mq4m3yAoW8E', 'Johnny Depp, Kenneth Branagh, Daisy Ridley'),
('No oneâ€™s child', 'Nicije dete', NULL, 'Vuk Rsumovic', '95 min', 2014, 'At late eighties, a boy has been found in mountains of Bosnia and Herzegovina. Nobody found out how he came in wild, nor if animals fed and raised him.', 'Serbia,Croatia', 'http://www.imdb.com/title/tt3059656', 'https://www.rottentomatoes.com/m/nicije_dete', NULL, 'Denis Muric, Milos Timotijevic, Pavle Cemerikic'),
('Pretty Village, Pretty Flame', 'Lepa Sela Lepo Gore', NULL, 'Srdjan Dragojevic', '150min', 1996, 'Domaci film', 'Srbija, BIH', 'http://www.imdb.com/title/tt0116860/?ref_=ttrel_rel_tt', 'https://www.rottentomatoes.com/m/pretty_village_pretty_flame/', '', 'Dragan Bjelogrlic'),
('Silence', 'Tisina', NULL, 'Martin Scorsese', '161 min', 2016, 'The story of two Catholic missionaries (Andrew Garfield and Adam Driver) who face the ultimate test of faith when they travel to Japan in search of their missing mentor (Liam Neeson) - at a time when Catholicism was outlawed and their presence orbidden.', 'Mexico,USA', 'http://www.imdb.com/title/tt0490215', 'https://www.rottentomatoes.com/m/silent', NULL, '/'),
('Star Wars: The Last Jedi', 'STAR WARS: POSLEDNJI DŽEDAJI', 'star_wars', 'Rian Johnson', '150 min', 2017, 'Rej je zakoracila u jedan veci svet u „Star Wars – Budjenje Sile“ i nastavice svoje epsko putovanje s Finom, Poom i Lukom Skajvokerom u sledecem poglavlju Star Wars sage.', 'USA', 'http://www.imdb.com/title/tt2527336/?ref_=nv_sr_2', 'https://www.rottentomatoes.com/m/star_wars_the_last_jedi', 'https://www.youtube.com/embed/Q0CbN8sfihY', 'Daisy Ridley, John Boyega, Oscar Isaac, Lupita Nyong’o, Adam Driver'),
('The Greatest Showman', 'VELICANSTVENI SOUMEN', 'greatest_showman', 'Michael Gracey', '105 min', 2017, '“Velicanstveni soumen” je smeo i originalan mjuzikl koji slavi rodjenje sou biznisa i ostvarenje snova jednog coveka.', 'USA', 'http://www.imdb.com/title/tt1485796/', 'https://www.rottentomatoes.com/m/the_greatest_showman_2017', 'https://www.youtube.com/embed/AXCTMGYUg9A', 'Hugh Jackman, Zac Efron, Michelle Williams, Zendaya, Rebecca Ferguson'),
('The Other Side of Everything', 'Druga strana svega', NULL, 'Mila Turajlic', '100 min', 2017, 'For Serbian filmmaker Mila Turajlic, a locked door in her mother\'s apartment in Belgrade provides the gateway to both her remarkable family history and her country\'s tumultuous political inheritance. ', 'Serbia', 'http://www.imdb.com/title/tt7218812 ', 'https://www.rottentomatoes.com/m/the_other_side_of_everything', NULL, 'Srbijanka Turajlic,Mila Turajlic'),
('We will be the world champions', 'Bicemo prvaci sveta', NULL, 'Darko Bajic', '127 min', 2015, 'The story about the founders of the famous Yugoslav Basketball School and the first gold medal at the Championships in Ljubljana in 1970, is based on real events and is dedicated to personalities who have contributed to the emergence and development of basketball in their country.', 'Serbia', 'http://www.imdb.com/title/tt4160726', 'https://www.rottentomatoes.com/m/bicemo_prvaci_sveta', NULL, 'Milos Bikovic,Aleksandar Radojicic,Strahinja Blazic'),
('Wonder', 'Cudo', 'wonder', 'Steve Chbosky', '113 min', 2017, 'Bazirano na bestseleru New York Timesa, Cudo je neverovatna inspirativna topla prica o Augustu Pullmanu, decaku s teskim genetskim poremecajem zbog kojeg od rodjenja ima unakazeno lice i glavu.', 'USA', 'http://www.imdb.com/title/tt2543472/', 'https://www.rottentomatoes.com/m/wonder', 'https://www.youtube.com/embed/ngiK1gQKgK8', 'Jacob Tremblay, Julia Roberts, Owen Wilson');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `place` varchar(30) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `location` varchar(200) DEFAULT NULL,
  `hall` varchar(50) DEFAULT NULL,
  `gpsLat` double DEFAULT NULL,
  `gpsLon` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `XPKLocations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `place`, `address`, `location`, `hall`, `gpsLat`, `gpsLon`) VALUES
(1, 'Beograd', 'Milentija Popovica 9', 'Sava Centar', 'Velika Sala', 44.808326, 20.432124),
(2, 'Beograd', 'Milentija Popovica 9', 'Sava Centar', 'Mala Sala', 44.808326, 20.432124),
(3, 'Beograd', 'Kolarceva 6', 'Kulturni Centar Beograd', 'Dvorana Kulturnog Centra', 44.815493, 20.460344),
(4, 'Beograd', 'Bulevar Mihajla Pupina 4', 'Cineplexx Usce', 'Sala 1', 44.81552, 20.437147),
(5, 'Beograd', 'Bulevar Mihajla Pupina 4', 'Cineplexx Usce', 'Sala 2', 44.81552, 20.437147),
(6, 'Beograd', 'Bulevar Mihajla Pupina 4', 'Cineplexx Usce', 'Sala 3', 44.81552, 20.437147),
(7, 'Beograd', 'Bulevar Mihajla Pupina 4', 'Cineplexx Usce', 'Sala 4', 44.81552, 20.437147),
(8, 'Beograd', 'Bulevar Mihajla Pupina 4', 'Cineplexx Usce', 'Sala 5', 44.81552, 20.437147),
(9, 'Beograd', 'Makedonska 22', 'Dom Omladine Beograda', 'Dvorana Doma Omladine', 44.815695, 20.462877),
(10, 'Herceg Novi', 'Kanli Kula', 'Tvrdjava Kanli Kula', 'Amfiteatar', 42.452195, 18.538031),
(11, 'Herceg Novi', 'Setaliste Pet Danica', 'Bioskop Forte Mare', 'Terasa', 42.449922, 18.535849);

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `message` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `projection`
--

DROP TABLE IF EXISTS `projection`;
CREATE TABLE IF NOT EXISTS `projection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `festival` varchar(50) NOT NULL,
  `film` varchar(50) NOT NULL,
  `ticketsCnt` int(11) DEFAULT NULL,
  `ticketPrice` int(11) NOT NULL,
  `location` int(11) NOT NULL,
  `projectionDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `XIF1Projections` (`festival`),
  KEY `XIF2Projections` (`film`),
  KEY `XIF3Projections` (`location`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projection`
--

INSERT INTO `projection` (`id`, `festival`, `film`, `ticketsCnt`, `ticketPrice`, `location`, `projectionDate`) VALUES
(2, 'Beogradski festival', 'Matilda', 230, 500, 8, '2018-01-02 00:00:00'),
(3, 'Beogradski festival', 'Jumanji: Welcome to the Jungle', 300, 530, 5, '2018-01-02 00:00:00'),
(4, 'Filmski Festival Herceg Novi', 'Wonder', 130, 650, 10, '2018-03-01 00:00:00'),
(5, 'Beogradski festival', 'Wonder', 250, 350, 9, '2018-01-16 19:32:20'),
(13, 'Omladinski', 'Atomski zdesna', 600, 350, 9, '2018-01-25 00:00:00'),
(14, 'Martovski festival', 'Atomski zdesna', 300, 350, 1, '2018-04-04 00:00:00'),
(15, 'Festival Autorskog Filma', 'Pretty Village, Pretty Flame', 300, 250, 9, '2017-11-29 00:00:00'),
(16, 'Festival Autorskog Filma', 'Silence', 300, 260, 3, '2017-11-30 00:00:00'),
(17, 'Festival Autorskog Filma', 'Wonder', 600, 260, 1, '2017-11-30 00:00:00'),
(18, 'Martovski festival', 'Pretty Village, Pretty Flame', 380, 350, 3, '2018-03-29 00:00:00'),
(19, 'Martovski festival', 'Dama Pik', 600, 350, 1, '2018-03-31 00:00:00'),
(20, 'Martovski festival', 'Malena', 600, 400, 7, '2018-03-31 00:00:00'),
(21, 'Filmski Festival Herceg Novi', 'The Other Side of Everything', 800, 450, 11, '2018-08-06 00:00:00'),
(22, 'Beogradski festival', 'Pretty Village, Pretty Flame', 1000, 450, 7, '2018-10-19 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `rates`
--

DROP TABLE IF EXISTS `rates`;
CREATE TABLE IF NOT EXISTS `rates` (
  `username` varchar(20) NOT NULL,
  `film` varchar(50) NOT NULL,
  `rate` int(11) DEFAULT NULL,
  PRIMARY KEY (`username`,`film`),
  KEY `film` (`film`),
  KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rates`
--

INSERT INTO `rates` (`username`, `film`, `rate`) VALUES
('milani', 'Matilda', 6),
('milani', 'Wonder', 6);

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
CREATE TABLE IF NOT EXISTS `reservation` (
  `code` char(10) NOT NULL,
  `date` date DEFAULT NULL,
  `ticketsCnt` int(11) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `projection` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '3' COMMENT '0-active, 1-sold, 2-rejected, 3-unconfirmed',
  PRIMARY KEY (`code`),
  UNIQUE KEY `XPKReservation` (`code`),
  KEY `XIF1Reservation` (`username`),
  KEY `reservation_fk2` (`projection`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`code`, `date`, `ticketsCnt`, `username`, `projection`, `state`) VALUES
('ABCDEWWWQ', '2018-01-01', 3, 'milani', 4, 1),
('BHSRIPGYLV', '2018-01-16', 2, 'milani', 2, 1),
('HLRKQOPISB', '2018-01-21', 3, 'mijatn', 21, 3),
('JUQAKSAFFP', '2018-01-21', 3, 'mijatn', 13, 3),
('KLAVRCSESW', '2018-01-21', 3, 'mijatn', 14, 3),
('MTMQYGFPRN', '2018-01-16', 2, 'milani', 5, 1),
('XYZABCBBBB', '2018-01-16', 3, 'milani', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(20) NOT NULL,
  `ime` varchar(20) DEFAULT NULL,
  `prezime` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `blocked` int(11) DEFAULT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `XPKUser` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `ime`, `prezime`, `password`, `telephone`, `email`, `type`, `blocked`) VALUES
('mijatn', 'Nemanja', 'Mijatovic', 'NemanjA12!', '1234567', 'mijat@gmail.com', 'visitor', 0),
('milani', 'Milan', 'Ilic', 'BeoGrad1!', '0616639563', 'ilic.m@gmail.com', 'visitor', 0),
('novikorisnik', 'Novi ', 'Korisnik', 'KorisniK11!', '1655189', 'email@email.com', 'waiting', 0),
('urosv', 'Uros', 'Veselinovic', 'sifra123', NULL, 'ure@gmail.com', 'admin', 0),
('vasa', 'Nikola', 'Vasovic', 'MirijevO1!', '061123456', 'vasa@vasa.com', 'seller', 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_fk1` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_fk2` FOREIGN KEY (`film`) REFERENCES `film` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `message_fk1` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `projection`
--
ALTER TABLE `projection`
  ADD CONSTRAINT `projection_fk1` FOREIGN KEY (`festival`) REFERENCES `festival` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `projection_fk2` FOREIGN KEY (`film`) REFERENCES `film` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `projection_fk3` FOREIGN KEY (`location`) REFERENCES `locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rates`
--
ALTER TABLE `rates`
  ADD CONSTRAINT `rates_fk1` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rates_fk2` FOREIGN KEY (`film`) REFERENCES `film` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `reservation_fk1` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reservation_fk2` FOREIGN KEY (`projection`) REFERENCES `projection` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MILAN
 */
@Entity
@Table(name = "rates")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rates.findAll", query = "SELECT r FROM Rates r")
    , @NamedQuery(name = "Rates.findByUsername", query = "SELECT r FROM Rates r WHERE r.ratesPK.username = :username")
    , @NamedQuery(name = "Rates.findByFilm", query = "SELECT r FROM Rates r WHERE r.ratesPK.film = :film")
    , @NamedQuery(name = "Rates.findByRate", query = "SELECT r FROM Rates r WHERE r.rate = :rate")})
public class Rates implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RatesPK ratesPK;
    @Column(name = "rate")
    private Integer rate;
    @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private User user;
    @JoinColumn(name = "film", referencedColumnName = "name", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Film film;

    public Rates() {
    }

    public Rates(RatesPK ratesPK) {
        this.ratesPK = ratesPK;
    }

    public Rates(String username, String film) {
        this.ratesPK = new RatesPK(username, film);
    }

    public RatesPK getRatesPK() {
        return ratesPK;
    }

    public void setRatesPK(RatesPK ratesPK) {
        this.ratesPK = ratesPK;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ratesPK != null ? ratesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rates)) {
            return false;
        }
        Rates other = (Rates) object;
        if ((this.ratesPK == null && other.ratesPK != null) || (this.ratesPK != null && !this.ratesPK.equals(other.ratesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beans.Rates[ ratesPK=" + ratesPK + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MILAN
 */
@Entity
@Table(name = "comments")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Comments.findAll", query = "SELECT c FROM Comments c")
    , @NamedQuery(name = "Comments.findByUsername", query = "SELECT c FROM Comments c WHERE c.commentsPK.username = :username")
    , @NamedQuery(name = "Comments.findByFilm", query = "SELECT c FROM Comments c WHERE c.commentsPK.film = :film")})
public class Comments implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CommentsPK commentsPK;
    @Lob
    @Size(max = 65535)
    @Column(name = "comment")
    private String comment;
    @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private User user;
    @JoinColumn(name = "film", referencedColumnName = "name", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Film film;

    public Comments() {
    }

    public Comments(CommentsPK commentsPK) {
        this.commentsPK = commentsPK;
    }

    public Comments(String username, String film) {
        this.commentsPK = new CommentsPK(username, film);
    }

    public CommentsPK getCommentsPK() {
        return commentsPK;
    }

    public void setCommentsPK(CommentsPK commentsPK) {
        this.commentsPK = commentsPK;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (commentsPK != null ? commentsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Comments)) {
            return false;
        }
        Comments other = (Comments) object;
        if ((this.commentsPK == null && other.commentsPK != null) || (this.commentsPK != null && !this.commentsPK.equals(other.commentsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beans.Comments[ commentsPK=" + commentsPK + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author MILAN
 */
@Entity
@Table(name = "projection")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Projection.findAll", query = "SELECT p FROM Projection p")
    , @NamedQuery(name = "Projection.findById", query = "SELECT p FROM Projection p WHERE p.id = :id")
    , @NamedQuery(name = "Projection.findByTicketsCnt", query = "SELECT p FROM Projection p WHERE p.ticketsCnt = :ticketsCnt")
    , @NamedQuery(name = "Projection.findByProjectionDate", query = "SELECT p FROM Projection p WHERE p.projectionDate = :projectionDate")})
public class Projection implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "ticketsCnt")
    private Integer ticketsCnt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "projectionDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date projectionDate;
    @JoinColumn(name = "festival", referencedColumnName = "name")
    @ManyToOne(optional = false)
    private Festival festival;
    @JoinColumn(name = "film", referencedColumnName = "name")
    @ManyToOne(optional = false)
    private Film film;
    @JoinColumn(name = "location", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Locations location;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projection")
    private List<Reservation> reservationList;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ticketPrice")
    private int ticketPrice;
    
    public Projection() {
    }

    public Projection(Integer id) {
        this.id = id;
    }

    public Projection(Integer id, Date projectionDate) {
        this.id = id;
        this.projectionDate = projectionDate;
    }
    
    public void sellTickets(int count){       
        ticketsCnt -= count;
    }

    public Integer getId() {
        return id;
    }
    
    public boolean isPassed() { return projectionDate.before(new Date()); }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTicketsCnt() {
        return ticketsCnt;
    }

    public void setTicketsCnt(Integer ticketsCnt) {
        this.ticketsCnt = ticketsCnt;
    }

    public Date getProjectionDate() {
        return projectionDate;
    }

    public void setProjectionDate(Date projectionDate) {
        this.projectionDate = projectionDate;
    }

    public Festival getFestival() {
        return festival;
    }

    public void setFestival(Festival festival) {
        this.festival = festival;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public Locations getLocation() {
        return location;
    }

    public void setLocation(Locations location) {
        this.location = location;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Projection)) {
            return false;
        }
        Projection other = (Projection) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beans.Projection[ id=" + id + " ]";
    }

    public int getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(int ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    @XmlTransient
    public List<Reservation> getReservationList() {
        return reservationList;
    }

    public void setReservationList(List<Reservation> reservationList) {
        this.reservationList = reservationList;
    }
    
}

package beans;

import beans.util.JsfUtil;
import beans.util.PaginationHelper;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.TypedQuery;
import org.primefaces.event.SelectEvent;

@Named("projectionController")
@SessionScoped
public class ProjectionController implements Serializable {

    private Projection current;
    private DataModel items = null;
    @EJB
    private beans.ProjectionFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private int ticketsForSell;
    private int ticketsCntForReservation;
    private List<Projection> projectionList;
    
    public void initProjectionList(){
        projectionList = ejbFacade.findAll();
    }

    public List<Projection> getProjectionList() {
        return projectionList;
    }

    public void setProjectionList(List<Projection> projectionList) {
        this.projectionList = projectionList;
    }
    
    private Date newProjectionDate;

    public Date getNewProjectionDate() {
        return newProjectionDate;
    }

    public void setNewProjectionDate(Date newProjectionDate) {
        this.newProjectionDate = newProjectionDate;
    }
    
    public ProjectionController() {
    }

    public void cancelProjection(Projection p){
         MessageController messageController = (MessageController) FacesContext.getCurrentInstance().getApplication()
            .getELResolver().getValue(FacesContext.getCurrentInstance().getELContext(), null, "messageController");
         
        // notify users
        for(Reservation r: p.getReservationList()){
            Message msg = new Message();
            msg.setUsername(r.getUsername());
            String text = "Postovani, projekcija filma '" + p.getFilm().getName() + "' zakazana za " + p.getProjectionDate() + " je otkazana.";
            if(r.isSold()) 
                text += "Mozete doci na blagajnu po Vas novac.";
            msg.setMessage(text);
            messageController.setSelected(msg);
            messageController.create();
        }
        
        current = p;
        performDestroy();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Uspesno", " otkazana projekcija."));     
    }
    
    public void changeProjection(){
        Projection p = current;
        MessageController messageController = (MessageController) FacesContext.getCurrentInstance().getApplication()
            .getELResolver().getValue(FacesContext.getCurrentInstance().getELContext(), null, "messageController");
         
        // notify users
        for(Reservation r: p.getReservationList()){
            Message msg = new Message();
            msg.setUsername(r.getUsername());
            String text = "Postovani, projekcija filma '" + p.getFilm().getName() + "' zakazana za " + p.getProjectionDate() + " je pomerena za " + newProjectionDate + ". Hvala na razumevanju.";
            msg.setMessage(text);
            messageController.setSelected(msg);
            messageController.create();
        }
        
        current.setProjectionDate(newProjectionDate);
        update();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Uspesno", " odlozena projekcija za " + newProjectionDate));
            
    }
    
    public void createReservation(){
        ReservationController reservationController = (ReservationController) FacesContext.getCurrentInstance().getApplication()
            .getELResolver().getValue(FacesContext.getCurrentInstance().getELContext(), null, "reservationController");
        UserController userController = (UserController) FacesContext.getCurrentInstance().getApplication()
            .getELResolver().getValue(FacesContext.getCurrentInstance().getELContext(), null, "userController");
       
        if(ticketsCntForReservation > current.getFestival().getMaxRez()){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Prekoracili ste max broj karata za rezervaciju: " + ticketsCntForReservation + "."));
            return;
        }
        if(ticketsCntForReservation > current.getTicketsCnt()){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Nema dovoljno karata. Ostalo je jos: " + current.getTicketsCnt() + "."));
            return;
        }
        
       if(reservationController.findByProjectionAndUser(current, userController.getSelected()).size() > 0){
           FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Vec ste rezervisali karte za datu projekciju."));
            return;
       }
       
       if(reservationController.findIsUserBlocked(userController.getSelected())){
           FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Blokirani ste u sistemu rezervacija!."));
            return;
       }
        
       String code = reservationController.createUniqueCode();
       Reservation res = new Reservation(code);
        res.setDate(new Date());    res.setProjection(current);   res.setTicketsCnt(ticketsCntForReservation);  res.setState(3); res.setUsername(userController.getSelected());
        
        reservationController.setSelected(res);
        reservationController.create();
        
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Uspesno poslata rezervacija. Vas KOD: " + code));
    }

    
    // dodavanje nove projekcije
    private Festival newFestival;
    private Locations newLocation;
    private Film newFilm;
    private Date newDate;
    private int newTicketsCnt, newTicketsPrice;

    public void checkDate(SelectEvent event){
        if(newDate.before(newFestival.getStartDate()) || newDate.after(newFestival.getEndDate())){
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.YYYY");
            String msg = "Projekcija mora biti za vreme odrzavanja festivala " + newFestival.getName() + ". Izmedju " + format.format(newFestival.getStartDate()) + " i " + format.format(newFestival.getEndDate());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Greska", msg));
            return;
        }
    }
    
    public void createNewProjection(){
        if(newDate.before(newFestival.getStartDate()) || newDate.after(newFestival.getEndDate())){
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.YYYY");
            String msg = "Projekcija mora biti za vreme odrzavanja festivala " + newFestival.getName() + ". Izmedju " + format.format(newFestival.getStartDate()) + " i " + format.format(newFestival.getEndDate());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Greska", msg));
            return;
        }
        
        if(newLocation == null){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Greska", "Mora se uneti lokacija projekcije!"));
            return;
        }
        
        Projection p = new Projection();
        p.setFestival(newFestival); p.setFilm(newFilm); p.setLocation(newLocation);
        p.setTicketPrice(newTicketsPrice); p.setTicketsCnt(newTicketsCnt); p.setProjectionDate(newDate);
        current = p;
        create();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Uspesno", "Zakazana projekcija za film: " + newFilm.getName()));        
    }
    
    public Festival getNewFestival() {
        return newFestival;
    }

    public void setNewFestival(Festival newFestival) {
        this.newFestival = newFestival;
    }

    public Locations getNewLocation() {
        return newLocation;
    }

    public void setNewLocation(Locations newLocation) {
        this.newLocation = newLocation;
    }

    
    public Film getNewFilm() {
        return newFilm;
    }

    public void setNewFilm(Film newFilm) {
        this.newFilm = newFilm;
    }

    public Date getNewDate() {
        return newDate;
    }

    public void setNewDate(Date newDate) {
        this.newDate = newDate;
    }

    public int getNewTicketsCnt() {
        return newTicketsCnt;
    }

    public void setNewTicketsCnt(int newTicketsCnt) {
        this.newTicketsCnt = newTicketsCnt;
    }

    public int getNewTicketsPrice() {
        return newTicketsPrice;
    }

    public void setNewTicketsPrice(int newTicketsPrice) {
        this.newTicketsPrice = newTicketsPrice;
    }
    
    
    public int getTicketsCntForReservation() {
        return ticketsCntForReservation;
    }

    public void setTicketsCntForReservation(int ticketsCntForReservation) {
        this.ticketsCntForReservation = ticketsCntForReservation;
    }
    
    public Projection getSelected() {
        if (current == null) {
            current = new Projection();
            selectedItemIndex = -1;
        }
        return current;
    }

    public void setSelected(Projection p) {
        current = p;
    }

    public int getTicketsForSell() {
        return ticketsForSell;
    }

    public void setTicketsForSell(int ticketsForSell) {
        this.ticketsForSell = ticketsForSell;
    }

    private ProjectionFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Projection) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }
    
    public void sellTickets(){
        if(current.getTicketsCnt() >= ticketsForSell){
            current.sellTickets(ticketsForSell);
            update();            
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO, "Uspesno prodato " + ticketsForSell + " karata!", ""));
        }else
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage  (FacesMessage.SEVERITY_ERROR,"", "Nema dovoljno ulaznica!"));
    }

    public String prepareCreate() {
        current = new Projection();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Projection) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Projection) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Projection getProjection(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = Projection.class)
    public static class ProjectionControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ProjectionController controller = (ProjectionController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "projectionController");
            return controller.getProjection(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Projection) {
                Projection o = (Projection) object;
                return getStringKey(o.getId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Projection.class.getName());
            }
        }

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author MILAN
 */
@Stateless
public class ScenesFacade extends AbstractFacade<Scenes> {

    @PersistenceContext(unitName = "FilmFestivalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ScenesFacade() {
        super(Scenes.class);
    }
    
}

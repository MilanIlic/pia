package beans;

import beans.util.JsfUtil;
import beans.util.PaginationHelper;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpSession;

@ApplicationScoped
@Named
public class UserController implements Serializable {

    private User current;
    private DataModel items = null;
    @EJB
    private beans.UserFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private String username = "";
    private String password = "";
    private String newPassword;
    private String ime, prezime, telefon, email;
    private List<User> usersList;
    public String getIme() {  return ime; }

    public void setIme(String ime) { this.ime = ime; }

    public String getPrezime() { return prezime; }

    public void setPrezime(String prezime) { this.prezime = prezime; }

    public String getTelefon() { return telefon; }

    public void setTelefon(String telefon) { this.telefon = telefon; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }
    
    public String getNewPassword() { return newPassword; }

    public void setNewPassword(String newPassword) { this.newPassword = newPassword; }
    
    public void initUsersList(){
        usersList = ejbFacade.findAll();
        usersList.removeIf(new Predicate<User>(){
            @Override
            public boolean test(User t) {
                return !t.getType().equals("waiting");
            }        
        });        
    }
    
    public void onMessageClose(Message m){
         MessageController messageController = (MessageController) FacesContext.getCurrentInstance().getApplication()
    .getELResolver().getValue(FacesContext.getCurrentInstance().getELContext(), null, "messageController");
         
        messageController.setSelected(m);
        messageController.destroy();
         
        updateMessagesList(m);        
    }
    
    public void updateMessagesList(Message m){
        current.removeMessage(m); 
    }
    
    public void confirmRegistration(User user){
        user.setType("visitor");
        current = user;
        update();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Uspesno", " potvrdili registraciju korisnika."));                
    }

    public List<User> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<User> usersList) {
        this.usersList = usersList;
    }
    
    public String register(){
        username = username.trim();
        User user = getFacade().find(username);
        if(user == null){
            if(checkPassword(password)){
                if(password.equals(newPassword)){
                    user = new User(username);
                    user.setPassword(password); user.setBlocked(0); user.setType("waiting"); user.setTelephone(telefon); user.setEmail(email);
                    user.setIme(ime); user.setPrezime(prezime);
                    
                    current = user;
                    create();
                    current = null;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Uspesno", " ste se registrovali. Ceka se odobrenje Administratora."));
                }else
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Pogresne lozinke", "Lozinke se ne poklapaju."));
            }
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Greska pri registraciji", "Username je vec u upotrebi."));
            
        }
        password = ""; newPassword = ""; telefon = ""; email = ""; ime = ""; prezime = "";
        return null;
    }
    
    public void checkUserExists(){
        username = username.trim();
        User user = getFacade().find(username);
        if(user != null)
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Greska pri registraciji", "Username je vec u upotrebi."));
        else 
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Slobodan username", ""));
    }
            
    public String changePassword(){
        User user = getFacade().find(username);

        if (user != null) {
            if (user.getPassword().equals(password)) {
                if(checkPassword(newPassword)){
                    user.setPassword(newPassword);
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Uspesno", "promenjena lozinka!"));
                    current = user;
                    update();   
                    current = null;
                    return "/index";
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Greska pri prijavljivanju", "Pogresna lozinka!"));
                password = "";
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Greska pri prijavljivanju", "Pogresan username!"));
            username = "";  password = ""; newPassword = "";
        }          
        return null;
    }
    
    public boolean checkPassword(String pass){
        if(pass == null || pass.isEmpty()) return false;
        pass = pass.trim();
        
        String special = "#*.!?$";
        int count = pass.length();
        int specCnt = 0, upCnt = 0, lowCnt = 0, numCnt = 0;
        
        if(!Character.isUpperCase(pass.charAt(0))){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Greska", "Prvi znak lozinke mora biti veliko slovo!"));
            return false;
        }
        
        for(int i = 0; i<count; i++){
            if(Character.isUpperCase(pass.charAt(i))) upCnt++;
            if(Character.isLowerCase(pass.charAt(i))) lowCnt++;
            if(Character.isDigit(pass.charAt(i))) numCnt++;
            if(special.indexOf(pass.charAt(i)) != -1) specCnt++;
        }
        if(upCnt >= 2 &&
            lowCnt >= 3 &&
            numCnt >= 1 &&
            specCnt >= 1){
          
            return true;
        }
        
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Greska", "Lozinka mora da ima min 2 velika i 3 mala slova, 1 numerik i 1 specijalan karakter '#*.!?$'!"));
        return false;   
    }
    
    public UserController() {
    }

    public User getSelected() {
        if (current == null) {
            current = new User();
            selectedItemIndex = -1;
        }
        return current;
    }

    private UserFacade getFacade() {
        return ejbFacade;
    }

    public String login() {
        FacesMessage message = null;
        boolean loggedIn = false;
        User user = getFacade().find(username);

        if (user != null) {
            if(user.getType().equals("waiting")){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Postovani, jos uvek se ceka odobrenje administratora. Hvala na razumevanju!", username)); 
                return null;
            }
            if (user.getPassword().equals(password)) {
                current = user;
                loggedIn = true;
                message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Dobrodosli", username);
                FacesContext.getCurrentInstance().addMessage(null, message);
                username = "";  password = "";
                user.setEntityManager(getFacade().getEntityManager());
                return user.getType();
            } else {
                message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Greska pri prijavljivanju", "Pogresna lozinka!");
                password = "";
            }
        } else {
            loggedIn = false;
            message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Greska pri prijavljivanju", "Pogresan username!");
            username = "";  password = "";
        }
        
        FacesContext.getCurrentInstance().addMessage(null, message);    
        return null;
    }

    public String logout() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        session.invalidate();
        username = ""; password = "";
        return "/index";
    }

    
    
    public void updateRatesList(){
        TypedQuery<Rates> query = getFacade().getEntityManager().createQuery("SELECT r FROM Rates r WHERE r.ratesPK.username = :username", Rates.class).setParameter("username", current.getUsername());
        current.setRatesList(query.getResultList());        
    }
    
    public boolean isRated(Film film){
        updateRatesList();
        for(Rates r: current.getRatesList())
            if (r.getFilm().getName().equals(film.getName()))
                return true;
        return false;
    }
    
    public String getUsername() {
        return username;
    }

    public User getUser() {
        return current;
    }

    public String home() {
        if (current != null) {
            return "/" + current.getType();
        } else {
            return null;
        }
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (User) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new User();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (User) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (User) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("UserDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public User getUser(java.lang.String id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = User.class)
    public static class UserControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            UserController controller = (UserController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "userController");
            return controller.getUser(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof User) {
                User o = (User) object;
                return getStringKey(o.getUsername());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + User.class.getName());
            }
        }

    }

}

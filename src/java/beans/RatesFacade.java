/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import beans.Rates;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author MILAN
 */
@Stateless
public class RatesFacade extends AbstractFacade<Rates> {

    @PersistenceContext(unitName = "FilmFestivalPU")
    private EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    public RatesFacade() {
        super(Rates.class);
    }
    
}

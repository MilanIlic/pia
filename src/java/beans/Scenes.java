/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MILAN
 */
@Entity
@Table(name = "scenes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Scenes.findAll", query = "SELECT s FROM Scenes s")
    , @NamedQuery(name = "Scenes.findById", query = "SELECT s FROM Scenes s WHERE s.id = :id")
    , @NamedQuery(name = "Scenes.findByImage", query = "SELECT s FROM Scenes s WHERE s.image = :image")
    , @NamedQuery(name = "Scenes.findByFilm", query = "SELECT s FROM Scenes s WHERE s.film = :film")})
public class Scenes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 40)
    @Column(name = "image")
    private String image;
    @Size(max = 40)
    @Column(name = "film")
    private String film;

    public Scenes() {
    }

    public Scenes(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFilm() {
        return film;
    }

    public void setFilm(String film) {
        this.film = film;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Scenes)) {
            return false;
        }
        Scenes other = (Scenes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beans.Scenes[ id=" + id + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.primefaces.event.RateEvent;

/**
 *
 * @author MILAN
 */
@Entity
@Table(name = "film")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Film.findAll", query = "SELECT f FROM Film f")
    , @NamedQuery(name = "Film.findByName", query = "SELECT f FROM Film f WHERE f.name = :name")
    , @NamedQuery(name = "Film.findBySerbianName", query = "SELECT f FROM Film f WHERE f.serbianName = :serbianName")
    , @NamedQuery(name = "Film.findByPicture", query = "SELECT f FROM Film f WHERE f.picture = :picture")
    , @NamedQuery(name = "Film.findByDirector", query = "SELECT f FROM Film f WHERE f.director = :director")
    , @NamedQuery(name = "Film.findByDuration", query = "SELECT f FROM Film f WHERE f.duration = :duration")
    , @NamedQuery(name = "Film.findByYearFilm", query = "SELECT f FROM Film f WHERE f.yearFilm = :yearFilm")
    , @NamedQuery(name = "Film.findByCountry", query = "SELECT f FROM Film f WHERE f.country = :country")
    , @NamedQuery(name = "Film.findByImdb", query = "SELECT f FROM Film f WHERE f.imdb = :imdb")
    , @NamedQuery(name = "Film.findByYoutube", query = "SELECT f FROM Film f WHERE f.youtube = :youtube")
    , @NamedQuery(name = "Film.findByCasts", query = "SELECT f FROM Film f WHERE f.casts = :casts")})
public class Film implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "name")
    private String name;
    @Size(max = 40)
    @Column(name = "serbianName")
    private String serbianName;
    @Size(max = 40)
    @Column(name = "picture")
    private String picture;
    @Size(max = 40)
    @Column(name = "director")
    private String director;
    @Size(max = 30)
    @Column(name = "duration")
    private String duration;
    @Column(name = "yearFilm")
    private Integer yearFilm;
    @Lob
    @Size(max = 65535)
    @Column(name = "aboutFilm")
    private String aboutFilm;
    @Size(max = 40)
    @Column(name = "country")
    private String country;
    @Size(max = 100)
    @Column(name = "imdb")
    private String imdb;
    @Size(max = 100)
    @Column(name = "youtube")
    private String youtube;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "casts")
    private String casts;
    @Size(max = 150)
    @Column(name = "rotten")
    private String rotten;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "film")
    private List<Projection> projectionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "film")
    private List<Comments> commentsList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "film")
    private List<Rates> ratesList;

    public Film() {
    }

    public Film(String name) {
        this.name = name;
    }

    public Film(String name, String casts) {
        this.name = name;
        this.casts = casts;
    }

    public String getName() {
        return name;
    }

    @Transient
    private EntityManager em;

    void setEntityManager(EntityManager entityManager) {
        em = entityManager;
    }

    public void updateRatesList() {
        if (em != null) {
            TypedQuery<Rates> query = em.createQuery("SELECT r FROM Rates r WHERE r.ratesPK.film = :film", Rates.class).setParameter("film", getName());
            setRatesList(query.getResultList());
        }
    }

    public void updateCommentsList() {
        if (em != null) {
            TypedQuery<Comments> query = em.createQuery("SELECT c FROM Comments c WHERE c.commentsPK.film = :film", Comments.class).setParameter("film", getName());
            setCommentsList(query.getResultList());
        }
    }

    public int getRates() {
        int sum = 0, cnt = ratesList.size();
        for (Rates r : ratesList) {
            sum += r.getRate();
        }
        if (cnt == 0) {
            return 0;
        } else {
            return sum / cnt;
        }
    }

    public boolean isRated(User user) {
        boolean watched = false;
        // check did passed user watch this film
        for (Projection p : getProjectionList()) {
            for (Reservation r : p.getReservationList()) {
                if (r.getUsername().getUsername().equals(user.getUsername()) && r.isSold() && r.isPassed()) {
                    watched = true;
                }
            }
        }

        updateRatesList();

        if (user.getType().equals("visitor") && watched) {
            for (Rates r : ratesList) {
                if (r.getUser().getUsername().equals(user.getUsername())) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    public boolean isCommented(User user) {
        boolean watched = false;
        // check did passed user watch this film
        for (Projection p : getProjectionList()) {
            for (Reservation r : p.getReservationList()) {
                if (r.getUsername().getUsername().equals(user.getUsername()) && r.isSold() && r.isPassed()) {
                    watched = true;
                }
            }
        }

        updateCommentsList();

        if (user.getType().equals("visitor") && watched) {
            for (Comments c : commentsList) {
                if (c.getUser().getUsername().equals(user.getUsername())) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSerbianName() {
        return serbianName;
    }

    public void setSerbianName(String serbianName) {
        this.serbianName = serbianName;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Integer getYearFilm() {
        return yearFilm;
    }

    public void setYearFilm(Integer yearFilm) {
        this.yearFilm = yearFilm;
    }

    public String getAboutFilm() {
        return aboutFilm;
    }

    public void setAboutFilm(String aboutFilm) {
        this.aboutFilm = aboutFilm;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getImdb() {
        return imdb;
    }

    public void setImdb(String imdb) {
        this.imdb = imdb;
    }

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

    public String getCasts() {
        return casts;
    }

    public void setCasts(String casts) {
        this.casts = casts;
    }

    @XmlTransient
    public List<Projection> getProjectionList() {
        return projectionList;
    }
    
    @XmlTransient
    public List<Projection> getFutureProjectionList() {
        List<Projection> futureList = projectionList;
        
        futureList.removeIf(new Predicate<Projection>(){
            @Override
            public boolean test(Projection t) {
                return t.getProjectionDate().before(new Date());
            }            
        });
        
        return futureList;
    }

    public void setProjectionList(List<Projection> projectionList) {
        this.projectionList = projectionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (name != null ? name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Film)) {
            return false;
        }
        Film other = (Film) object;
        if ((this.name == null && other.name != null) || (this.name != null && !this.name.equals(other.name))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beans.Film[ name=" + name + " ]";
    }

    @XmlTransient
    public List<Comments> getCommentsList() {
        updateCommentsList();
        return commentsList;
    }

    public void setCommentsList(List<Comments> commentsList) {
        this.commentsList = commentsList;
    }

    public String getRotten() {
        return rotten;
    }

    public void setRotten(String rotten) {
        this.rotten = rotten;
    }

    @XmlTransient
    public List<Rates> getRatesList() {
        updateRatesList();
        return ratesList;
    }

    public void setRatesList(List<Rates> ratesList) {
        this.ratesList = ratesList;
    }

}

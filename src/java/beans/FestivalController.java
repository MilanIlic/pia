package beans;

import beans.util.JsfUtil;
import beans.util.PaginationHelper;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

@Named("festivalController")
@SessionScoped
public class FestivalController implements Serializable {

    private Festival current;
    private DataModel items = null;
    @EJB
    private beans.FestivalFacade ejbFacade;
    private PaginationHelper pagination;

    private MapModel simpleModel;
    private int selectedItemIndex;

    private Date startDate, endDate;
    private String searchFestivalName, searchFilmName;
    private List<Festival> festivalsList = new ArrayList<>(), upcomingFestivals = new ArrayList<>();
    private List<Projection> projections = new ArrayList<>();
    private boolean showProjections;

    private List<Film> filmsList = new ArrayList<>();

    public boolean isShowProjections() {
        return showProjections;
    }

    public void filmsClose() {
        filmsList.clear();
    }

    public void saveFilms() {
        FilmController filmController = (FilmController) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(FacesContext.getCurrentInstance().getELContext(), null, "filmController");

        filmsList.removeIf(new Predicate<Film>(){
            @Override
            public boolean test(Film t) {
                return filmController.getFacade().find(t.getName()) != null;
            }            
        });
        
        for (Film film : filmsList) {
            filmController.setSelected(film);
            filmController.create();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Kreiran film " + film.getName(), ""));
        }
        filmsClose();
    }

    public List<Festival> getAllFestivals() {
        return ejbFacade.findAll();
    }

    private static final String[] marker_icon = {"blue.png", "green.png", "lightblue.png",
        "pink.png", "purple.png", "red.png",
        "yellow.png", "ylw-pushpin.png"};

    public void init() {
        ArrayList<Locations> locations = new ArrayList<>();
        ArrayList<String> locationsNames = new ArrayList<>();
        int indx = 0;

        simpleModel = new DefaultMapModel();

        for (Projection p : current.getProjectionList()) {
            if (locationsNames.indexOf(p.getLocation().getLocation()) == -1) {
                simpleModel.addOverlay(new Marker(new LatLng(p.getLocation().getGpsLat(), p.getLocation().getGpsLon()), p.getLocation().getLocation(), null, "/resources/icon/" + marker_icon[indx]));
                locations.add(p.getLocation());
                locationsNames.add(p.getLocation().getLocation());

                indx = (indx + 1) % marker_icon.length;
            }
        }
    }

    public void onFilmClose(String f) {
        filmsList.removeIf(new Predicate<Film>() {
            @Override
            public boolean test(Film t) {
                System.out.println(t.getName() + "  " + f + " " + filmsList.size());
                return t.getName().equals(f);
            }
        });

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Zatvoren film " + f, ""));
    }

    private List<Locations> festivalLocations = new ArrayList<>();

    public void onFestivalSelect(Festival f) {
        LocationsController locationsController = (LocationsController) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(FacesContext.getCurrentInstance().getELContext(), null, "locationsController");

        festivalLocations = locationsController.findAllByPlace(f.getPlace());

    }

    public List<Locations> getFestivalLocations() {
        return festivalLocations;
    }

    public void initUpcomingFestivals() {
        upcomingFestivals = ejbFacade.findAll();

        upcomingFestivals.sort(new Comparator<Festival>() {
            @Override
            public int compare(Festival o1, Festival o2) {
                return o1.getStartDate().compareTo(o2.getStartDate());
            }
        });

        upcomingFestivals.removeIf(new Predicate<Festival>() {
            @Override
            public boolean test(Festival t) {
                return t.getEndDate().before(new Date());
            }
        });

        LinkedList<Festival> festivals = new LinkedList<>();
        for (int i = 0; i < upcomingFestivals.size() && i < 5; i++) {
            festivals.add(upcomingFestivals.get(i));
        }

        upcomingFestivals = festivals;
    }

    public List<Festival> getUpcomingFestivals() {
        return upcomingFestivals;
    }

    public void setUpcomingFestivals(List<Festival> upcomingFestivals) {
        this.upcomingFestivals = upcomingFestivals;
    }

    public void initFestivalsList() {
        festivalsList = ejbFacade.findAll();
    }

    private UploadedFile file;

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public void uploadFestivals(){
        try{
        if (file != null && !file.getFileName().isEmpty()) {
            FacesMessage message = new FacesMessage("Uspesno ", " je upload-ovan fajl " + file.getFileName());
            FacesContext.getCurrentInstance().addMessage(null, message);
            if (file.getFileName().endsWith(".json")) {
                parseJsonFestivalFile();
            } else if (file.getFileName().endsWith(".csv")) {
                parseCsvFestivalFile();
            }
        }
        }catch(IOException io){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Greska prilikom parsiranja fajla ", file.getFileName()));
        }
    }

    public void uploadFilms() {
        
        try{
        if (file != null && !file.getFileName().isEmpty()) {
            FacesMessage message = new FacesMessage("Uspesno ", " je upload-ovan fajl " + file.getFileName());
            FacesContext.getCurrentInstance().addMessage(null, message);
            if (file.getFileName().endsWith(".json")) {
                parseJsonFilmFile();
            } else if (file.getFileName().endsWith(".csv")) {
                parseCsvFilmFile();
            }
        }
        } catch(IOException io){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Greska prilikom parsiranja fajla ", file.getFileName()));
        }
    }

 

    private List<Festival> fileFestivals;

    public List<Festival> getFileFestivals() {
        return fileFestivals;
    }

    public void setFileFestivals(List<Festival> fileFestivals) {
        this.fileFestivals = fileFestivals;
    }

    public void festivalsClose() {
        fileFestivals.clear();
    }

    public void saveFestivals() {
        FestivalController festivalController = (FestivalController) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(FacesContext.getCurrentInstance().getELContext(), null, "festivalController");
        
        fileFestivals.removeIf(new Predicate<Festival>(){
            @Override
            public boolean test(Festival t) {
                return ejbFacade.find(t.getName()) != null;
            }            
        });
        
        for (Festival festival : fileFestivals) {
            festivalController.current = festival;
            create();
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Uspesno", "festivali su dodati."));
        
        festivalsClose();
    }
    
    private List<Locations> fileLocations;

    public List<Locations> getFileLocations() {
        return fileLocations;
    }

    public void setFileLocations(List<Locations> fileLocations) {
        this.fileLocations = fileLocations;
    }

    public void locationsClose() {
        fileLocations.clear();
    }

    public void saveLocations() {
        LocationsController locationsController = (LocationsController) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(FacesContext.getCurrentInstance().getELContext(), null, "locationsController");
        
        
        for (Locations locations : fileLocations) {
            locationsController.setSelected(locations);
            locationsController.create();
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Uspesno", "lokacije su dodate."));
        locationsClose();
    }
    
    
       public void parseCsvFilmFile() throws IOException{
         Reader reader = new InputStreamReader(file.getInputstream());
        CSVParser parser = CSVParser.parse(reader, CSVFormat.RFC4180);
        fileFestivals = new ArrayList<>();
        try {
            boolean first = true;
            for (CSVRecord csvRecord : parser) {
                if(first){
                    first = false;
                    continue;
                }
                if(csvRecord.get(0).startsWith("-")) break;
                
                Film f = new Film(csvRecord.get(1));
               
                f.setSerbianName(csvRecord.get(0)); f.setName(csvRecord.get(0));
                f.setYearFilm(Integer.parseInt(csvRecord.get(2))); f.setAboutFilm(csvRecord.get(3));
                f.setDirector(csvRecord.get(4)); f.setCasts(csvRecord.get(5));
                f.setDuration(csvRecord.get(6)); f.setCountry(csvRecord.get(7));
                f.setImdb(csvRecord.get(8)); f.setRotten(csvRecord.get(9));                
                filmsList.add(f);
            }
        } catch (NumberFormatException e) {
           FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Greska u parsiranju fajla.", ""));
        }
    }

    public void parseCsvFestivalFile() throws IOException {
        Reader reader = new InputStreamReader(file.getInputstream());
        CSVParser parser = CSVParser.parse(reader, CSVFormat.RFC4180);
        fileFestivals = new ArrayList<>();
        try {
            boolean first = true;
            for (CSVRecord csvRecord : parser) {
                if(first){
                    first = false;
                    continue;
                }
                if(csvRecord.get(0).startsWith("-")) break;
                
                Festival f = new Festival(csvRecord.get(0));
                DateFormat format = new SimpleDateFormat("dd/MM/YYYY");
                Date startDate1 = format.parse(csvRecord.get(1)), endDate1 = format.parse(csvRecord.get(2));

                f.setAbout(csvRecord.get(4));
                f.setStartDate(startDate1);
                f.setEndDate(endDate1);
                f.setPlace(csvRecord.get(3));
                
                fileFestivals.add(f);
            }
        } catch (java.text.ParseException e) {
           FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Greska u parsiranju fajla.", ""));
        }
    }

    public void parseJsonFestivalFile() throws IOException {
        JSONParser parser = new JSONParser();

        fileFestivals = new ArrayList<>();
        fileLocations = new ArrayList<>();
        try {

            JSONObject jsonFile = (JSONObject) parser.parse(new InputStreamReader(new ByteArrayInputStream(file.getContents())));

            JSONArray festivals = (JSONArray) jsonFile.get("Festivals");
            JSONArray locations = (JSONArray) jsonFile.get("Locations");

            for (Object o : festivals) {
                JSONObject festival = (JSONObject) o;

                String name = (String) festival.get("Festival");
                System.out.println(name);

                String startDate = (String) festival.get("StartDate");
                System.out.println(startDate);

                String endDate = (String) festival.get("EndDate");
                System.out.println(endDate);

                String place = (String) festival.get("Place");
                System.out.println(place);

                String about = (String) festival.get("About");
                System.out.println(about);

                Festival f = new Festival(name);
                DateFormat format = new SimpleDateFormat("dd/MM/YYYY");
                Date startDate1 = format.parse(startDate), endDate1 = format.parse(endDate);

                f.setAbout(about);
                f.setStartDate(startDate1);
                f.setEndDate(endDate1);
                f.setPlace(place);
                fileFestivals.add(f);
            }

            for (Object o : locations) {
                JSONObject location = (JSONObject) o;

                String place = (String) location.get("Place");
                System.out.println(place);

                JSONArray locs = (JSONArray) location.get("Location");

                for (Object c : locs) {
                    JSONObject placeName = (JSONObject) c;
                    String name = (String) placeName.get("Name");
                    System.out.println(name);
                    String coords = (String) placeName.get("Coordinates");
                    System.out.println(coords);
                    
                    double lat = Double.parseDouble(coords.split(",")[0]), lon = Double.parseDouble(coords.split(",")[1]); 
                    Locations loc = new Locations();
                    loc.setPlace(place); loc.setLocation(name);
                    loc.setGpsLat(lat); loc.setGpsLon(lon); 
                    
                    fileLocations.add(loc);
                }
            }

        } catch (Exception p) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Greska u parsiranju fajla.", ""));
        }
    }

    public List<Film> getFilmsList() {
        return filmsList;
    }

    public void setFilmsList(List<Film> filmsList) {
        this.filmsList = filmsList;
    }

    public void parseJsonFilmFile() throws IOException {
        JSONParser parser = new JSONParser();

        try {

            JSONObject jsonFile = (JSONObject) parser.parse(new InputStreamReader(new ByteArrayInputStream(file.getContents())));

            JSONArray movies = (JSONArray) jsonFile.get("Movies");

            for (Object o : movies) {
                JSONObject movie = (JSONObject) o;

                String name = (String) movie.get("Title");
                System.out.println(name);

                String originalName = (String) movie.get("OriginalTitle");
                System.out.println(originalName);

                Long year = (Long) movie.get("Year");
                System.out.println(year);

                String about = (String) movie.get("Summary");
                System.out.println(about);

                String director = (String) movie.get("Director");
                System.out.println(director);

                String casts = (String) movie.get("Stars");
                System.out.println(casts);

                Long duration = (Long) movie.get("Runtime");
                System.out.println(duration);

                String country = (String) movie.get("Country");
                System.out.println(country);

                String imdb = (String) movie.get("Link1");
                System.out.println(imdb);

                String rotten = (String) movie.get("Link2");
                System.out.println(rotten);

                Film film = new Film(originalName);
                film.setSerbianName(name);
                film.setDuration(duration.intValue() + " min");
                film.setYearFilm(year.intValue());
                film.setDirector(director);
                film.setAboutFilm(about);
                film.setCasts(casts);
                film.setCountry(country);
                film.setImdb(imdb);
                film.setRotten(rotten);

                filmsList.add(film);
            }

        } catch (ParseException p) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Greska u parsiranju fajla.", ""));
        }
    }

    private String nazivNew, mestoOdrzavanjaNew, aboutFestivalNew;
    private Date startDateNew, endDateNew;
    private int maxRezNew;
    private Festival newFestival;

    public Festival getNewFestival() {
        return newFestival;
    }

    public void setNewFestival(Festival newFestival) {
        this.newFestival = newFestival;
    }

    public void checkDates(SelectEvent event) {
        if (startDateNew != null && endDateNew != null) {
            if (startDateNew.after(endDateNew)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Datumi nisu pravilno uneti", "Kraj festivala mora biti nakon pocetka!"));
            }
        }
    }

    public String createNewFestival() {

        Festival festival = ejbFacade.find(nazivNew);
        if (festival != null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Neispravno ime", "Festival sa unetim imenom vec postoji!"));
            return null;
        }
        if (startDateNew != null && endDateNew != null) {
            if (startDateNew.after(endDateNew)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Datumi nisu pravilno uneti", "Kraj festivala mora biti nakon pocetka!"));
                return null;
            }
        }

        festival = new Festival(nazivNew);
        festival.setPlace(mestoOdrzavanjaNew);
        festival.setAbout(aboutFestivalNew);
        festival.setStartDate(startDateNew);
        festival.setEndDate(endDateNew);
        festival.setMaxRez(maxRezNew);

        current = festival;
        create();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Uspesno kreiran festival", nazivNew));
        newFestival = festival;
        current = null;

        return "/addProjections";
    }

    public int getMaxRezNew() {
        return maxRezNew;
    }

    public void setMaxRezNew(int maxRezNew) {
        this.maxRezNew = maxRezNew;
    }

    public String getNazivNew() {
        return nazivNew;
    }

    public void setNazivNew(String nazivNew) {
        this.nazivNew = nazivNew;
    }

    public String getMestoOdrzavanjaNew() {
        return mestoOdrzavanjaNew;
    }

    public void setMestoOdrzavanjaNew(String mestoOdrzavanjaNew) {
        this.mestoOdrzavanjaNew = mestoOdrzavanjaNew;
    }

    public String getAboutFestivalNew() {
        return aboutFestivalNew;
    }

    public void setAboutFestivalNew(String aboutFestivalNew) {
        this.aboutFestivalNew = aboutFestivalNew;
    }

    public Date getStartDateNew() {
        return startDateNew;
    }

    public void setStartDateNew(Date startDateNew) {
        this.startDateNew = startDateNew;
    }

    public Date getEndDateNew() {
        return endDateNew;
    }

    public void setEndDateNew(Date endDateNew) {
        this.endDateNew = endDateNew;
    }

    public MapModel getSimpleModel() {
        return simpleModel;
    }

    public void onDateSelect(SelectEvent event) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Date Selected", format.format(event.getObject())));

        filterFestivals();
    }

    public void filterFestivals() {
        festivalsList = ejbFacade.findAll();
        showProjections = false;
        if (searchFestivalName != null && !searchFestivalName.isEmpty()) {
            festivalsList.removeIf(new Predicate<Festival>() {
                @Override
                public boolean test(Festival t) {
                    return !t.getName().toLowerCase().startsWith(searchFestivalName.toLowerCase());
                }
            });
        }
        
        festivalsList.removeIf(new Predicate<Festival>(){
            @Override
            public boolean test(Festival t) {
                return t.getEndDate().before(new Date());
            }            
        });
        

        if (startDate != null && endDate != null) {
            festivalsList.removeIf(new Predicate<Festival>() {
                @Override
                public boolean test(Festival t) {
                    return !((startDate.before(t.getStartDate()) || t.getStartDate().equals(startDate)) && (endDate.after(t.getEndDate()) || t.getEndDate().equals(endDate)));
                }
            });
        }

        if (searchFilmName != null && !searchFilmName.isEmpty()) {
            showProjections = true;
            festivalsList.removeIf(new Predicate<Festival>() {
                @Override
                public boolean test(Festival t) {
                    for (Projection p : t.getProjectionList()) {
                        if (p.getFilm().getName().startsWith(searchFilmName)) {
                            return false;
                        }
                    }
                    return true;
                }
            });
            
            projections.clear();

            for (Festival f : festivalsList) {
                for (Projection p : f.getProjectionList()) {
                    if (p.getFilm().getName().startsWith(searchFilmName)) {
                        projections.add(p);
                    }
                }
            }
        }
    }

    public String getSearchFilmName() {
        return searchFilmName;
    }

    public void setSearchFilmName(String searchFilmName) {
        this.searchFilmName = searchFilmName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getSearchFestivalName() {
        return searchFestivalName;
    }

    public void setSearchFestivalName(String searchFestivalName) {
        this.searchFestivalName = searchFestivalName;
    }

    public List<Festival> getFestivalsList() {
        return festivalsList;
    }

    public void setFestivalsList(List<Festival> festivalsList) {
        this.festivalsList = festivalsList;
    }

    public FestivalController() {
    }

    public Festival getSelected() {
        if (current == null) {
            current = new Festival();
            selectedItemIndex = -1;
        }
        
        return current;
    }

    private FestivalFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public List<Projection> getProjections() {
        return projections;
    }

    public void setProjections(List<Projection> projections) {
        this.projections = projections;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Festival) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareView(Festival f) {
        current = f;
        current.setEntityManager(ejbFacade.getEntityManager());
        return "/festival/View";
    }

    public String prepareCreate() {
        current = new Festival();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Festival) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("FestivalUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Festival) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("FestivalDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Festival getFestival(java.lang.String id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = Festival.class)
    public static class FestivalControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            FestivalController controller = (FestivalController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "festivalController");
            return controller.getFestival(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Festival) {
                Festival o = (Festival) object;
                return getStringKey(o.getName());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Festival.class.getName());
            }
        }

    }

}

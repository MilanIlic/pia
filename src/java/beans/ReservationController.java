package beans;

import beans.util.JsfUtil;
import beans.util.PaginationHelper;
import java.util.List;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.TypedQuery;

@Named("reservationController")
@ApplicationScoped
public class ReservationController implements Serializable {

    

    private Reservation current;
    private DataModel items = null;
    @EJB
    private beans.ReservationFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private String searchCode;
    private String searchUser;
    
    public ReservationController() {
    }

    public Reservation getSelected() {
        if (current == null) {
            current = new Reservation();
            selectedItemIndex = -1;
        }
        return current;
    }
    
    public void setSelected(Reservation res) {
        current = res;
    }
    
    public void activateReservation() {
        current.setState(0);
        getFacade().edit(current);
        FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO, "Uspesno aktivirana rezervacija!", ""));
    }
    
    public String generateRandomCode(){
       String symbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
       char[] buf = new char[10];
       Random random = new Random();
        for (int idx = 0; idx < buf.length; ++idx)
            buf[idx] = symbols.charAt(random.nextInt(symbols.length()));
        return new String(buf);    
    }
    
    public String createUniqueCode(){
        String code = generateRandomCode();
        
        Reservation r = ejbFacade.find(code);
        while(r != null){
            code = generateRandomCode();
            r = ejbFacade.find(code);
        }
        return code;
    }
    
    public void sellTickets(Reservation res) {
        current = res;
        current.setState(1);
        getFacade().edit(current);
        FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_INFO, "Uspesno prodate " + res.getTicketsCnt() + " rezervisane karte!", ""));
    }
    
    public void cancelReservation(Reservation res) {
        current = res;
        current.setState(2);
        getFacade().edit(current);
    }

    private ReservationFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }
    
    public List<Reservation> findByProjectionAndUser(Projection p, User user){
         TypedQuery<Reservation> query = getFacade().getEntityManager().createQuery("SELECT r FROM Reservation r WHERE r.projection = :idproj AND r.username = :user", Reservation.class).setParameter("idproj", p).setParameter("user", user);
         return query.getResultList();
    }
    
    public boolean findIsUserBlocked(User user) {    
        Date currentDate = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.DATE, -2);
        currentDate = cal.getTime();
        
        System.out.println("Datum: " + currentDate);
        TypedQuery<Reservation> query = getFacade().getEntityManager().createQuery("SELECT r FROM Reservation r WHERE r.username = :user AND r.state = 0 AND r.date < :date", Reservation.class).setParameter("user", user).setParameter("date", currentDate);
         
        System.out.println("Blokiran: " + query.getResultList().size());
        return query.getResultList().size() >= 3;
    }


    public void prepareList() {
        recreateModel();
        getItems();
    }

    public String prepareView() {
        current = (Reservation) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Reservation();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Reservation) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("ReservationUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Reservation) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage("Uspesno obrisana rezervacija.");
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, "Greska prilikom brisanja rezervacije.");
        }
    }
    
    public String getSearchCode() {
        return searchCode;
    }

    public void setSearchCode(String searchCode) {
        this.searchCode = searchCode;
    }

    public String getSearchUser() {
        return searchUser;
    }

    public void setSearchUser(String searchUser) {
        this.searchUser = searchUser;
    }

    public void sellTickets(){
        int count = current.getTicketsCnt();
        if(count <= current.getProjection().getTicketsCnt()){
            current.getProjection().sellTickets(count);
            performDestroy();
        }        
    }
    
    public void searchFilter(){
        List<Reservation> reservationsList = getFacade().findAll();
        
        reservationsList.removeIf(new Predicate<Reservation>(){
            @Override
            public boolean test(Reservation t) {
                if(searchUser != null && !searchUser.isEmpty()){
                    String[] parts = searchUser.split("\\s+");
                    if(!(t.getUsername().getIme().startsWith(parts[0]) || t.getUsername().getPrezime().startsWith(parts[0]))) 
                        return true;
                    if(parts.length > 1){
                        if(!((t.getUsername().getIme().equals(parts[0]) && t.getUsername().getPrezime().startsWith(parts[1])) || (t.getUsername().getPrezime().equals(parts[0]) && t.getUsername().getIme().startsWith(parts[1]))))return true;
                    }
                }
                
                if(searchCode != null && !searchCode.isEmpty()){
                    return !t.getCode().startsWith(searchCode);
                }
                
                return false;
            }          
        });
        
        items = new ListDataModel(reservationsList);
    }
    
    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Reservation getReservation(java.lang.String id) {
        return ejbFacade.find(id);
    }

    
    @FacesConverter(forClass = Reservation.class)
    public static class ReservationControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ReservationController controller = (ReservationController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "reservationController");
            return controller.getReservation(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Reservation) {
                Reservation o = (Reservation) object;
                return getStringKey(o.getCode());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Reservation.class.getName());
            }
        }

    }

}

package beans;

import beans.util.JsfUtil;
import beans.util.PaginationHelper;
import java.io.File;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.naming.InitialContext;
import javax.persistence.TypedQuery;
import javax.transaction.UserTransaction;
import org.primefaces.event.RateEvent;

@Named("filmController")
@ApplicationScoped
public class FilmController implements Serializable {

    private Film current;
    private DataModel items = null;
    @EJB
    private beans.FilmFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private List<Film> filmsList;
    private List<String> scenes;

    private int rates;

    private Film newFilm = new Film();

    public Film getNewFilm() {
        return newFilm;
    }

    public void setNewFilm(Film newFilm) {
        this.newFilm = newFilm;
    }

    public void addNewFilm() {

        if (ejbFacade.find(newFilm.getName()) != null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Greska", "Film " + newFilm.getName() + " vec postoji!"));
            return;
        }
        current = newFilm;
        create();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Uspesno", "ste dodali film " + current.getName()));

        newFilm = new Film();
    }

    public void init() {
        scenes = new ArrayList<>();
        try{
            String path = Thread.currentThread().getContextClassLoader().getResource("images/" + current.getPicture() + "/scenes").getPath();
            File[] listOfFiles = (new File(path).listFiles());

            for (File f : listOfFiles) {
                scenes.add("images/" + current.getPicture() + "/scenes/" + f.getName());
            }
        }catch(NullPointerException e){ }

    }

    public List<Film> getAllFilms() {
        filmsList = ejbFacade.findAll();
        return filmsList;
    }

    public void setSelected(Film f) {
        current = f;
    }

    public int getRates() {

        if (current != null) {
            return current.getRates();
        }
        return 0;
    }

    public void setRates(int r) {
        rates = r;
    }

    public void onrate() {

        RatesController ratesController = (RatesController) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(FacesContext.getCurrentInstance().getELContext(), null, "ratesController");
        UserController userController = (UserController) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(FacesContext.getCurrentInstance().getELContext(), null, "userController");

        Rates givenRate = new Rates(userController.getSelected().getUsername(), current.getName());
        givenRate.setFilm(current);
        givenRate.setUser(userController.getSelected());
        givenRate.setRate(rates);

        ratesController.setSelected(givenRate);
        ratesController.create();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Uspesno", "ste ocenili film " + current.getName()));
    }

    private String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void oncomment() {

        CommentsController commentsController = (CommentsController) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(FacesContext.getCurrentInstance().getELContext(), null, "commentsController");
        UserController userController = (UserController) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(FacesContext.getCurrentInstance().getELContext(), null, "userController");

        Comments givenComments = new Comments(userController.getSelected().getUsername(), current.getName());
        givenComments.setFilm(current);
        givenComments.setUser(userController.getSelected());
        givenComments.setComment(comment);

        commentsController.setSelected(givenComments);
        commentsController.create();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Uspesno", "ste dodali komentar"));
    }

    public List<String> getScenes() {
        return scenes;
    }

    public FilmController() {
    }

    public Film getSelected() {
        if (current == null) {
            current = new Film();
            selectedItemIndex = -1;
        }
        return current;
    }

    public String prepareView(Film f) {
        current = f;
        current.setEntityManager(ejbFacade.getEntityManager());
        return "/film/View";
    }

    public FilmFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Film) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Film();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);

            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Film) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Film) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("FilmDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Film getFilm(java.lang.String id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = Film.class)
    public static class FilmControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            FilmController controller = (FilmController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "filmController");
            return controller.getFilm(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Film) {
                Film o = (Film) object;
                return getStringKey(o.getName());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Film.class.getName());
            }
        }

    }

}
